import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ConnectorService} from './connector.service';
import { ZonkyComponent } from './zonky/zonky.component';
import {HttpClientModule} from '@angular/common/http';
import {ButtonModule} from 'primeng/button';
import { FormsModule } from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    ZonkyComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ButtonModule,
    FormsModule,
    DropdownModule,
    BrowserAnimationsModule,
  ],
  providers: [
    ConnectorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
