import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';

@Injectable()
export class ConnectorService {

  public readonly RATING: string = 'rating__eq';
  public readonly FIELD: string = 'fields';
  public readonly AMOUNT: string = 'amount';
  public readonly HTTP_HEADERS: HttpHeaders = new HttpHeaders()
    .set('X-Size', '1000000');

  public readonly url: string = environment.backEndUrl;

  constructor (
    private http: HttpClient,
  ) { }

  public getLoan(rating: string): Observable<any[]> {
    return this.http.get<any[]>(this.url, {
      params: new HttpParams()
        .set(this.RATING, rating)
        .set(this.FIELD, this.AMOUNT),
      headers: this.HTTP_HEADERS
    });
  }
}

