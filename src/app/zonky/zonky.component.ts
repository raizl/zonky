import { Component, OnInit } from '@angular/core';
import {ConnectorService} from '../connector.service';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-zonky',
  templateUrl: './zonky.component.html',
  styleUrls: ['./zonky.component.css']
})
export class ZonkyComponent implements OnInit {

  public sum = 0;
  public roundedSum = 0;
  public rating = '';
  public isCounting = false;
  public ratingOptions: SelectItem[];

  constructor(
    private connectorService: ConnectorService
  ) {
    this.ratingOptions = [
      {label: 'A***', value: 'AAAAAA'},
      {label: 'A**', value: 'AAAAA'},
      {label: 'A*', value: 'AAAA'},
      {label: 'A++', value: 'AAA'},
      {label: 'AA+', value: 'AAE'},
      {label: 'A+', value: 'AA'},
      {label: 'AA', value: 'AE'},
      {label: 'A', value: 'A'},
      {label: 'B', value: 'B'},
      {label: 'C', value: 'C'},
      {label: 'D', value: 'D'}
    ];
  }

  ngOnInit() {

  }

  public countSum(rating: string) {
    this.sum = 0;
    let tempSum = 0;
    this.isCounting = true;
    return this.connectorService.getLoan(this.rating).toPromise()
      .then(
      res => {
        res.forEach(a => {
          tempSum += a.amount;
        });
        tempSum = tempSum / res.length;
      })
      .then( () => {
        this.isCounting = false;
        this.sum = tempSum;
        this.roundedSum = Math.round(tempSum);
    });

  }
}
