import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonkyComponent } from './zonky.component';

describe('ZonkyComponent', () => {
  let component: ZonkyComponent;
  let fixture: ComponentFixture<ZonkyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZonkyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonkyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
